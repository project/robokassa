<?php

define('ROBOKASSA_DEBUG_ERRORS', true);

function robokassa_prefillform($form_state){
   
	$form=array();
	 
	$form['amount'] = array(
		'#type' => 'textfield', 
		'#title' => t('Amount'), 
		'#default_value' => '', 
		'#size' => 10, 
		'#maxlength' => 12, 
		'#required' => TRUE
	);

	$form['memo'] = array(
		'#type' => 'textarea', 
		'#title' => t('Memo'), 
		'#default_value' => t('Payment to !sitename', array('!sitename' => variable_get('site_name', 'Drupal'))), 
		'#description' => t("Payment description."),
		'#required' => TRUE
	);

	$form['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Cerate payment'),
	);

	return $form;

}

function robokassa_prefillform_submit(&$form, $form_state){
	
	$payment=_robokassa_createpayment(array(
		'amount'=>$form_state['values']['amount'],
		'memo'=>$form_state['values']['memo'],
	));


	if(is_array($payment) && $payment['pid']>0){
		drupal_set_message(t("Please confirm payment details"));
		drupal_goto('robokassa/payment/'.$payment['pid']);
	}

}

function robokassa_merchantform($form_state, $payment){

	global $base_url;

	if(!is_array($payment) && is_integer($payment)){ // fetch payment info from DB
		$payment=robokassa_pid_load($payment);
	}

	if(!is_array($payment) || !($payment['pid']>0)){

		$form['error'] = array(
		  '#type' => 'item',
		  '#title' => t('Error'),
		  '#value' => t('Order you are going to pay for does not exist'),
		);

	}else{

		$form['#action'] = variable_get('robokassa_payment_url', ROBOKASSA_MERCHANT_URL);
		
		// interface data:

		$form['payment_id'] = array(
		  '#type' => 'item',
		  '#title' => t('Order #'),
		  '#value' => $payment['pid'],
		);

		$form['amount'] = array(
		  '#type' => 'item',
		  '#title' => t('Amount'),
		  '#value' => round($payment['amount'], 2),
		);

		$form['memo'] = array(
		  '#type' => 'item',
		  '#title' => t('Memo'),
		  '#value' => $payment['memo']
		);

		// merchant data...

		$form['MrchLogin'] = array(
			'#type' => 'hidden',
			'#value' => variable_get('robokassa_login', ''),
		);

		$form['InvId'] = array(
			'#type' => 'hidden',
			'#value' => $payment['pid'],
		);
	 
		$form['sInvDesc'] = array(
			'#type' => 'hidden',
			'#value' => $payment['memo'],
		);

		$form['OutSum'] = array(
			'#type' => 'hidden',
			'#value' => round($payment['amount'], 2),
		);

		$form['SignatureValue'] = array(
			'#type' => 'hidden',
			'#value' => md5(variable_get('robokassa_login', '').':'.round($payment['amount'], 2).':'.$payment['pid'].':'.variable_get('robokassa_secretkey', '')),
		);

		$form['IncCurrLabel'] = array(
			'#type' => 'hidden',
			'#value' => variable_get('robokassa_default_currency', 'BANKOCEAN2R'),
		);

		$form['Culture'] = array(
			'#type' => 'hidden',
			'#value' => 'ru',
		);



		$form['submit'] = array(
			'#type' => 'submit',
			'#value' => t('Pay now'),
		);

	}

	return $form;

}

function robokassa_success(){

	/*$created=time();

	$hash = sha1(md5($_POST['payment'].variable_get('robokassa_secretkey', '')));

	$pb_payment=_robokassa_parse_query($_POST['payment']);

	if(preg_match("/^[0-9]{1,10}$/", $pb_payment['order']) && $hash==$_POST['signature']){ // proccessing payment if only hash is valid

	   $payment=robokassa_pid_load($pb_payment['order']);
	   if(!is_array($payment)){
	      if(ROBOKASSA_DEBUG_ERRORS){
		     $f=fopen("/srv/sobesednik.ru/htdocs/sites/all/modules/robokassa/pblog.txt", "ab");
			 fwrite($f, date("m/d/Y H:i:s", $created).' return_url error: payment not found'."\n");
			 fwrite($f,var_export($_POST, true)."\n");
			 fclose($f);
		  }
		  return theme('robokassa_fail');
	   }
		
	   // check if payment already enrolled
	   if($payment['enrolled']>0){
	      if(ROBOKASSA_DEBUG_ERRORS){
		     $f=fopen("/srv/sobesednik.ru/htdocs/sites/all/modules/robokassa/pblog.txt", "ab");
			 fwrite($f,date("m/d/Y H:i:s", $created).' return_url error: payment already enrolled'."\n");
			 fwrite($f,var_export($_POST, true)."\n");
			 fclose($f);
		  }
		  return theme('robokassa_success');
	   }

	   $currency_settings = unserialize(variable_get('robokassa_currencies', serialize(_robokassa_GetDefCurSetts())));
	   $presc = $currency_settings["{$payment['currency']}"]['presc'];


	   if($pb_payment['amt']==round($payment['amount'], $presc) && $pb_payment['merchant']==$payment['payee_account'] && $pb_payment['ccy']==$payment['currency']){

		  // enroll payment
		  $result = db_query('UPDATE {robokassa} SET batch=\'%s\', payer_account=\'%s\', enrolled=%d WHERE pid=%d', $pb_payment['ref'], $pb_payment['sender_phone'], $created, $payment['pid']);
					
		  // fire hook
		  $payment['batch']=$pb_payment['ref'];
		  $payment['payer_account']=$pb_payment['sender_phone'];
		  $payment['enrolled']=$created;
		  module_invoke_all('robokassa', 'enrolled', $payment['pid'], $payment);

		  if(ROBOKASSA_DEBUG_ERRORS){
		     $f=fopen("/srv/sobesednik.ru/htdocs/sites/all/modules/robokassa/pblog.txt", "ab");
			 fwrite($f,date("m/d/Y H:i:s", $created).' return_url payment OK!'."\n");
			 fwrite($f,var_export($_POST, true)."\n");
			 fclose($f);
		  }

		  return theme('robokassa_success');

	   }else{ // you can also save invalid payments for debug purposes

	      if(ROBOKASSA_DEBUG_ERRORS){
		     $f=fopen("/srv/sobesednik.ru/htdocs/sites/all/modules/robokassa/pblog.txt", "ab");
			 fwrite($f,date("m/d/Y H:i:s", $created).' return_url error: fake data'."\n");
			 fwrite($f,var_export($_POST, true)."\n");
			 fclose($f);
		  }
		  return theme('robokassa_fail');

	   }


	}else{ // you can also save invalid payments for debug purposes

	   // uncomment code below if you want to log requests with bad hash
	   if(ROBOKASSA_DEBUG_ERRORS){
	      $f=fopen("/srv/sobesednik.ru/htdocs/sites/all/modules/robokassa/pblog.txt", "ab");
		  fwrite($f,date("m/d/Y H:i:s", $created).' return_url error: bad hash or payment_id'."\n");
		  fwrite($f,var_export($_POST, true)."\n");
		  fclose($f);
	   }
	   return theme('robokassa_fail');

	}*/

	 return theme('robokassa_success');

}

function robokassa_fail(){
	return theme('robokassa_fail');
}

function robokassa_status(){

	drupal_set_header('Content-type: text/html; charset=iso-8859-1');

	// check url
	$url=trim($_SERVER['REQUEST_URI'], '/');
	$alias=drupal_get_path_alias('robokassa/status');
	if($url!=$alias){
		   if(ROBOKASSA_DEBUG_ERRORS){
			  $f=fopen("/srv/sobesednik.ru/htdocs/sites/all/modules/robokassa/pblog.txt", "ab");
			  fwrite($f,date("m/d/Y H:i:s", $created).' bad sign'."\n");
			  fwrite($f,var_export($_POST, true)."\n");
			  fclose($f);
		   }
		   die("bad sign");
	}

	$created=time();


	$out_summ = $_REQUEST["OutSum"];
	$inv_id = $_REQUEST["InvId"];
	$crc = $_REQUEST["SignatureValue"];

	$crc = strtoupper($crc);

	$my_crc = strtoupper(md5("$out_summ:$inv_id:".variable_get('robokassa_secretkey', '')));

	if ($my_crc !=$crc || !preg_match("/^[0-9]{1,10}$/", $_REQUEST["InvId"]))
	{
	   // uncomment code below if you want to log requests with bad hash
	   if(ROBOKASSA_DEBUG_ERRORS){
	      $f=fopen("/srv/sobesednik.ru/htdocs/sites/all/modules/robokassa/pblog.txt", "ab");
		  fwrite($f,date("m/d/Y H:i:s", $created).' server_url error: bad hash or payment_id'."\n");
		  fwrite($f,var_export($_POST, true)."\n");
		  fclose($f);
	   }
	   die("bad sign\n");
	}

	   $payment=robokassa_pid_load($_REQUEST["InvId"]);
	   if(!is_array($payment)){
	      if(ROBOKASSA_DEBUG_ERRORS){
		     $f=fopen("/srv/sobesednik.ru/htdocs/sites/all/modules/robokassa/pblog.txt", "ab");
			 fwrite($f,date("m/d/Y H:i:s", $created).' server_url error: payment not found'."\n");
			 fwrite($f,var_export($_POST, true)."\n");
			 fclose($f);
		  }
		  die("bad InvId\n");
	   }

	   // check if payment already enrolled
	   if($payment['enrolled']>0){
	      if(ROBOKASSA_DEBUG_ERRORS){
		     $f=fopen("/srv/sobesednik.ru/htdocs/sites/all/modules/robokassa/pblog.txt", "ab");
			 fwrite($f,date("m/d/Y H:i:s", $created).' server_url error: payment already enrolled'."\n");
			 fwrite($f,var_export($_POST, true)."\n");
			 fclose($f);
		  }
		  die("bad sign\n");
	   }
	   
	   if($_REQUEST["OutSum"]==round($payment['amount'], 2)){

		  // enroll payment
		  $result = db_query('UPDATE {robokassa} SET  enrolled=%d WHERE pid=%d', $created, $payment['pid']);
					
		  // fire hook
		  $payment['enrolled']=$created;
		  module_invoke_all('robokassa', 'enrolled', $payment['pid'], $payment);

		  if(ROBOKASSA_DEBUG_ERRORS){
		     $f=fopen("/srv/sobesednik.ru/htdocs/sites/all/modules/robokassa/pblog.txt", "ab");
			 fwrite($f,date("m/d/Y H:i:s", $created).' server_url payment OK!'."\n");
			 fwrite($f,var_export($_POST, true)."\n");
			 fclose($f);
		  }

		  die("OK$inv_id\n");

	   }else{ // you can also save invalid payments for debug purposes

	      if(ROBOKASSA_DEBUG_ERRORS){
		     $f=fopen("/srv/sobesednik.ru/htdocs/sites/all/modules/robokassa/pblog.txt", "ab");
			 fwrite($f,date("m/d/Y H:i:s", $created).' server_url error: fake data'."\n");
			 fwrite($f,var_export($_POST, true)."\n");
			 fclose($f);
		  }
		  die();

	   }


}